# Mailvorlage für Personalwerk

Die Mailvorlage verwendet [mjml](https://mjml.io/), um ein Tabellenlayout zu generieren. Der Quellcode der Vorlage ist in [src](https://gitlab.com/pwrk/mailing/-/tree/main/src?ref_type=heads).

Die compilierte Version der Vorlage wird als Gitlab page veröffentlicht

[Mailvorlage](https://mailing.pwrk.dev/template.html)